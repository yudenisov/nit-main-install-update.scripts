set registry_file=IntSettings-HKCU-Desecure.reg

rem Install Internet Explorer
dism /online /enable-feature /feature-name: Internet-Explorer-Optional-amd64 /all
dism /online /enable-feature /feature-name: Internet-Explorer-Optional-amd32 /all

echo File %TEMP%\%registry_file%
%SystemRoot%\System32\reg.exe %TEMP%\%registry_file%
rem pause
