'******************************************************************************
' IntSettings-HKCU-Desecure.vbs
'
' This Script Executes Two the Command File 
'

' USAGE:
' %SystemRoot%\System32\cscript" //NOLOGO IntSettings-HKCU-Desecure.vbs
' The Script Returns:
'	Nothing
'
'
'******************************************************************************
Dim batFile1 		'Command File to be Run
Dim batFile2 		'Command File to be Run
Dim local_Path		'Local Path to Command File with Drive Letter
Dim local_File		'Full Command File Name
Dim Url			'Full URL Neme of the File in the Site
Dim pathCMD		'Path to CMD.EXE File
Dim wshShell, envVarProccess
Set wshShell = CreateObject( "WScript.Shell" )
Set envVarProccess = WshShell.Environment("PROCESS")
' *** Custom Parameters ***
local_Path = envVarProccess( "TEMP" ) & "\"			'Path To User Temprorary Directory
pathCMD = envVarProccess( "SystemRoot" ) & "\System32\"	'Path to Win64 CMD.exe File
batFile1="IntSettings-HKCU-Desecure.bat"
batFile2="InstallIE-Win10.bat"
' *** /Custom Parameters ***

' MSG Box

MsgBox "This file Install the Internet Explorer at Windows 10" & vbCrLf & "(if it not Existed) and Set Security Settings for it" & vbCrLf & vbCrLf & "���� ������ ������������� Internet Explorer � Windows 10 " & vbCrLf & "(���� �� �� ����������) � ��������� � ���� ���������� ������������", vbOkOnly
       
'/MSg Box

' *** Execute Downloaded Command File ***
Dim shApp
Set shApp = CreateObject( "Shell.Application" )
shApp.ShellExecute pathCMD & "cmd.exe", "/c " & local_Path & batFile1, pathCMD, "", 0
shApp.ShellExecute pathCMD & "cmd.exe", "/c " & local_Path & batFile2, pathCMD, "runas", 0
' /*** Execute Downloaded Command File ***
' The End of the script
