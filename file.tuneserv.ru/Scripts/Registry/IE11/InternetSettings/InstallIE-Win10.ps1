# Environment Variables
$http_protocol="http"
$http_host="localhost"
$http_port="10080"
$http_catalog="/Scripts/Registry/IE11/InternetSettings/"
$registry_file="IntSettings-HKCU-Desecure.reg"
$bat_file="IntSettings-HKCU-Desecure.bat"
$install_win10="InstallIE-Win10.bat"
$vbs_file="IntSettings-HKCU-Desecure.vbs"
$myhost=$http_protocol+"://"+$http_host+":"+$http_port+$http_catalog
Echo "Host: "$myhost
$local_file=$env:TEMP+"\"+$registry_file
Echo $local_file

$WebClient = New-Object System.Net.WebClient
$WebClient.DownloadFile($myhost+$registry_file, $env:TEMP+"\"+$registry_file)
$WebClient.DownloadFile($myhost+$bat_file, $env:TEMP+"\"+$bat_file)
$WebClient.DownloadFile($myhost+$install_win10, $env:TEMP+"\"+$install_win10)
$WebClient.DownloadFile($myhost+$vbs_file, $env:TEMP+"\"+$vbs_file)

$local_file=$env:TEMP+"\"+$vbs_file
$runfile = $env:SystemRoot+"\system32\cscript.exe"
$allargs =  @( "//Nologo", $local_file )
Echo $runfile $allargs
& $runfile $allargs

