<!--Threaded Script to .lnk Downloads -->
<?php
if (isset($_GET['file']) and file_exists($_GET['file']) and end(explode(".", $_GET['file'])) == "lnk") {
  $file1 = $_GET['file'];
  $ctype = mime_content_type(__DIR__."/".$file1);
  header('Content-Type: text/html; charset=utf-8');
  header("Content-Disposition: attachment; filename=".$file1);
  exit();
} else {
  echo "File not Found. Файл не найден.";
  exit();
}
?>