@echo om
rem *******************************************************
rem
rem NIT.REPAIR.cmd
rem This Command File Repairs Exponenta Admin Pack
rem Environment and Install Missing Modules
rem
rem PARAMETERS:	NONE
rem RETURNS:	0 if Success or Nothing To Download
rem		1 if Error Occur and Script Terminated
rem		2 if NIT System Update must Installed
rem SOURCE:	http://file.tuneserv.ru/Exponenta/NIT.REPAIR.cmd
rem
rem *******************************************************
@echo off

rem Add local Variables
setlocal enableextensions enabledelayedexpansion

set curdir=%CD%
set DEST_DIR=C:\NIT.SYSUPDATE

rem Set Common Host Downloads Varoables
set prefc=http
set hostc=file.tuneserv.ru
set portc=80
set uppathc=WinUpdate
set exppathc=Exponenta

rem define the Variables
set LMExecPolicy=HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\PowerShell\1\ShellIds\Microsoft.PowerShell
set CUExecPolicy=HKEY_CURRENT_USER\SOFTWARE\Microsoft\PowerShell\1\ShellIds\Microsoft.PowerShell

set FULLREP=FULLREPAIR.bat
set REVMON=ReverseMonitoringSetup.exe
set REVMONProduct="NIT Reverse Monitoring"
set LOADANDINST=load-and-install.bat
set LOADSYSUPDATE=Load-NIT-System-Update-Lite.vbs

set host=%prefc%://%hostc%:%portc%/%exppathc%
set host1=%prefc%://%hostc%:%portc%/%uppathc%

rem BEGIN

rem Create NIT.SYSUPDATE directory
mkdir %DEST_DIR%

:is_ChocolateyInstall
rem Check if Chocolatey Module is Installed
if defined ChocolateyInstall goto Success_ChocolateyInstall
echo Installing System Updates...
echo Download %LOADSYSUPDATE%...
%SystemRoot%\System32\bitsadmin /Transfer STEA_TRANSFER /DOWNLOAD /Priority FOREGROUND %host1%/%LOADSYSUPDATE% %DEST_DIR%\%LOADSYSUPDATE%
echo Run %LOADSYSUPDATE%...
%SystemRoot%\System32\cscript.exe //Nologo %DEST_DIR%\%LOADSYSUPDATE%
exit /b 2
:Success_ChocolateyInstall
echo Chocolatey Module is Install. Skip Installation
goto is_CurlInstall

:is_CurlInstall
rem Check if Curl Module is Installed
if exist %ChocolateyInstall%\bin\curl.exe goto Success_CurlInstall
echo Installing Curl...
cinst -y --ignore-checksums curl
:Success_CurlInstall
choco upgrade -y --ignore-checksums curl
echo Curl Module is Install. Skip Installation
goto is_WgetInstall

:is_WgetInstall
rem Check if Wget Module is Installed
if exist %ChocolateyInstall%\bin\wget.exe goto Success_WgetInstall
echo Installing Wget...
cinst -y --ignore-checksums wget
:Success_WgetInstall
choco upgrade -y --ignore-checksums wget
echo Wget Module is Install. Skip Installation
goto Setup_ExecutionPolicy

:Setup_ExecutionPolicy
rem Setup Powershell Execution Policy (Unrestricted)
echo Setting Local Machine Execution Policy...
%SystemRoot%\System32\reg.exe add "%LMExecPolicy%" /v "ExecutionPolicy" /t REG_SZ /d "Unrestricted" /f
echo Setting Current User Execution Policy...
%SystemRoot%\System32\reg.exe add "%CUExecPolicy%" /v "ExecutionPolicy" /t REG_SZ /d "Unrestricted" /f
goto Setup_FullRepair

:Setup_FullRepair
rem Download and Execute %FULLREP% Command File
del /F /Q %DEST_DIR%\%FULLREP%
echo Downloading %FULLREP% Command File...
curl %host%/%FULLREP% -o %DEST_DIR%\%FULLREP%
if not exist %DEST_DIR%\%FULLREP% goto UnSuccess_FullRepDownload
echo Executing %DEST_DIR%\%FULLREP%...
call %DEST_DIR%\%FULLREP%
goto Setup_ReverseMonitoring
:UnSuccess_FullRepDownload
echo Can't Download %FULLREP% File. Operation Terminated.
exit /b 1
goto Setup_ReverseMonitoring

:Setup_ReverseMonitoring
rem Download Reverse Monitoring Installer
echo Delete old %REVMON% file...
del /F /Q %DEST_DIR%\%REVMON%
echo Downloading %REVMON% File...
curl %host%/%REVMON% -o %DEST_DIR%\%REVMON%
if not exist %DEST_DIR%\%REVMON% goto UnSuccess_RevMon
rem echo Unistall %REVMON%...
rem wmic product where name=REVMONProduct call uninstall
echo Install %DEST_DIR%\%REVMON%...
%DEST_DIR%\%REVMON% /norestart /verysilent
goto Setup_loadandinst
:UnSuccess_RevMon
echo Can't Download %REVMON% File. Operation Terminated.
exit /b 1
goto Setup_loadandinst

:Setup_loadandinst
rem Download and Execute %LOADANDINST% Command File
del /F /Q %DEST_DIR%\%LOADANDINST%
echo Downloading %LOADANDINST% Command File...
curl %host%/%LOADANDINST% -o %DEST_DIR%\%LOADANDINST%
if not exist %DEST_DIR%\%LOADANDINST% goto UnSuccess_loadandinst
echo Executing %DEST_DIR%\%LOADANDINST%...
call %DEST_DIR%\%LOADANDINST%
goto End_Script
:UnSuccess_loadandinst
echo Can't Download %LOADANDINST% File. Operation Terminated.
exit /b 1
goto End_Script

:End_Script
rem END
exit /b 0