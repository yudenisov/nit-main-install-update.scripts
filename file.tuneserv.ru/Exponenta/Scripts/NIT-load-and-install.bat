@echo on
rem ***************************************************************************
rem
rem NIT-load-and-install.bat
rem
rem This file Downloads and Repair Main Exponenta Files and Plugins
rem on local computer
rem
rem ALGORITHM
rem This Command Script Install some packets over Private Chocolatey Repository
rem and some packets over Download and Install Scripts
rem ATTENTION!!!
rem This Script Needs LIBScript, HiddenStart, chocolatey, curl and wget 
rem installation
rem
rem PARAMETERS: NO
rem RETURN:	NO
rem
rem SOURCE: http://file.tuneserv.ru/Exponenta/Scripts/NIT-load-and-install.bat
rem
rem ***************************************************************************
@echo off

rem Initialization of Variables

SetLocal EnableExtensions EnableDelayedExpansion

rem Set Directories Path
set curdirforurl=%CD%
set home_dir=c:\Util
set DEST_DIR=C:\NIT.SYSUPDATE
set PUB1=C:\pub1

rem Initialization Download Variables

set http_pref1=http
set http_host1=file.tuneserv.ru
set http_port1=80
set http_dir1=/Exponenta/Distrib/bin/
set http_dir0=/Exponenta/

rem Derivatives Variables
set host=%http_pref1%://%http_host1%:%http_port1%%http_dir0%
set LocalFolder=%PUB1%\Distrib

rem wget Download a File

del /F /Q "%LocalFolder%\load-and-install.bat"

echo Download load-and-install.bat ...
"C:\Program Files\Hidden Start\hstart.exe" /NOCONSOLE /WAIT "wget %host%load-and-install.bat -O %LocalFolder%\load-and-install.bat -c -t 38 -w 120 -T 1800"

echo Install load-and-install.bat ...
"C:\Program Files\Hidden Start\hstart.exe" /NOCONSOLE /RUNAS /NOUAC "%LocalFolder%\load-and-install.bat"
