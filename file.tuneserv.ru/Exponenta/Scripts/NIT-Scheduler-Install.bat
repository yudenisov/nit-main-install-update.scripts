@echo off
rem Initiallize Script Variables environment

SetLocal EnableExtensions EnableDelayedExpansion

set curdirforurl=%CD%

rem Adjust Environment Variables for WGet

rem Settings Variables:

rem Installation Directory
call "C:\ProgramData\chocolatey\bin\RefreshEnv.cmd"

rem set PUB1=c:\pub1
rem set AdminT=c:\Elevation
rem set ELEVATION=c:\Elevation
rem set UTIL=c:\Util

rem Main Installation Hosts

set httphost1=file.tuneserv.ru
set httpMainFolder1=Exponenta
set httppref1=http
set httpport1=80
set httpuser1=
set httppassword1=

rem File to Downloads & Install
set Main_File1=NIT-Scheduler.msi
SET Main_Name1="NIT-Scheduler"

rem HTTP WebDAV Host
set host=%httppref1%://%httphost1%:%httpport1%/%httpMainFolder1%
rem Local Directory
set LocalFolder=%PUB1%\Util

cd /d "%LocalFolder%"


rem WGET Execution

DEL /F /Q "%LocalFolder%\%Main_File1%"
echo "Download %Main_File1%..."
wget.exe %host%/%Main_File1% -O "%LocalFolder%\%Main_File1%" -c -t 38 -w 120 -T 1800

rem Uninstall %Main_File1%

echo "Uninstall %Main_Name1% ..."
%SystemRoot%\System32\wbem\WMIC.EXE product where name=%Main_Name1% call uninstall

rem Install Threads Plugin
if not exist "%LocalFolder%\%Main_File1%" goto pass_Utils
echo "Install %LocalFolder%\%Main_File1%..."
rem "%LocalFolder%\%Main_File1%" /VERYSILENT /NOCANCEL
%SystemRoot%\system32\msiexec.exe /i "%LocalFolder%\%Main_File1%" /norestart /QN /L*V %TEMP%\%Main_File1%.log
DEL /F /Q "%LocalFolder%\%Main_File1%"
:pass_Utils


rem ChangeDirectory
cd /d %curdirforurl%

rem Exit of the Script
exit /b 0
