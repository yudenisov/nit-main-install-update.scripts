@echo off
rem Initiallize Script Variables environment

SetLocal EnableExtensions EnableDelayedExpansion

set curdirforurl=%CD%

rem Adjust Environment Variables for WGet

rem Settings Variables:

rem Installation Directory
call "C:\ProgramData\chocolatey\bin\RefreshEnv.cmd"

rem set PUB1=c:\pub1
rem set AdminT=c:\Elevation
rem set ELEVATION=c:\Elevation
rem set UTIL=c:\Util

rem Main Installation Hosts 1

set httphost1=ware.tuneserv.ru
set httpMainFolder1=Scripts/NIT-Schedule
set httppref1=http
set httpport1=80
set httpuser1=
set httppassword1=

rem File1 to Downloads & Install
set Main_File1=Task_Write.bat
SET Main_Name1=""

rem HTTP WebDAV Host
set host1=%httppref1%://%httphost1%:%httpport1%/%httpMainFolder1%
rem Main Installation Hosts 0

set httphost0=file.tuneserv.ru
set httpMainFolder0=Exponenta/Scripts
set httppref0=http
set httpport0=80
set httpuser0=
set httppassword0=

rem File0 to Downloads & Install
set Main_File0=NIT-Scheduler-Install.bat
SET Main_Name0=""

rem HTTP WebDAV Host
set host0=%httppref0%://%httphost0%:%httpport0%/%httpMainFolder0%
rem Local Directory
set LocalFolder=%PUB1%\Util

cd /d "%LocalFolder%"


rem WGET Execution

DEL /F /Q "%LocalFolder%\%Main_File0%"
echo "Download %Main_File0%..."
wget.exe %host0%/%Main_File0% -O "%LocalFolder%\%Main_File0%" -c -t 38 -w 120 -T 1800

rem Uninstall %Main_File0%

echo "Uninstall %Main_Name0% ..."
rem %SystemRoot%\System32\wbem\WMIC.EXE product where name=%Main_Name0% call uninstall

rem Run %Main_Name0% ...
if not exist "%LocalFolder%\%Main_File0%" goto pass_Utils
echo "Install %LocalFolder%\%Main_File0%..."
rem "%LocalFolder%\%Main_File0%" /VERYSILENT /NOCANCEL
rem %SystemRoot%\system32\msiexec.exe /i "%LocalFolder%\%Main_File0%" /norestart /QN /L*V %TEMP%\%Main_File0%.log
call "%LocalFolder%\%Main_File0%"
DEL /F /Q "%LocalFolder%\%Main_File0%"
:pass_Utils

rem WGET Execution & Download %Main_Name1%
DEL /F /Q "%LocalFolder%\%Main_File1%"
echo "Download %Main_File1%..."
wget.exe %host1%/%Main_File1% -O "%LocalFolder%\%Main_File1%" -c -t 38 -w 120 -T 1800

rem ChangeDirectory
cd /d %curdirforurl%

rem Exit of the Script
exit /b 0
