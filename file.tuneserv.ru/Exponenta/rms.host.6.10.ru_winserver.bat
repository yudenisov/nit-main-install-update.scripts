rem *******************************************************
rem
rem rms.host.6.10.ru_winserver.bat
rem This Script Installs the RMSHost Program with
rem WINSERVER Internet-ID Server
rem
rem *******************************************************
@echo off

rem Add local Variables

setlocal enableextensions enabledelayedexpansion

set curdir=%CD%

# Install RMS from Chocolatey

choco install rmshost.winserver -y --source http://ware.tuneserv.ru:8624/nuget/choco-feed/

# Repair RMS-Host autoload

sc config RManService start= auto
net start RManService
