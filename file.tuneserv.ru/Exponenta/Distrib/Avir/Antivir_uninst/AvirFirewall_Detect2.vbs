Option Explicit

Dim objSWbemServicesEx

Dim collSWbemObjectSet_AntiVirusProduct
Dim objSWbemObjectEx_AntiVirusProduct

Dim collSWbemObjectSet_FireWallProduct
Dim objSWbemObjectEx_FireWallProduct


Set objSWbemServicesEx = GetObject("WinMgmts:{impersonationLevel=impersonate}!\\.\root\SecurityCenter2")


Set collSWbemObjectSet_AntiVirusProduct = objSWbemServicesEx.ExecQuery("SELECT * From AntiVirusProduct")

If collSWbemObjectSet_AntiVirusProduct.Count <> 0 Then
    For Each objSWbemObjectEx_AntiVirusProduct In collSWbemObjectSet_AntiVirusProduct
        With objSWbemObjectEx_AntiVirusProduct
            WScript.Echo .instanceGuid & vbCrLf & .displayName & vbCrLf & .pathToSignedProductExe & vbCrLf & .pathToSignedReportingExe
        End With
    Next
Else
    WScript.Echo "Not found AntiVirus Products"
End If

Set collSWbemObjectSet_AntiVirusProduct = Nothing


Set collSWbemObjectSet_FireWallProduct = objSWbemServicesEx.ExecQuery("SELECT * From FireWallProduct")

If collSWbemObjectSet_FireWallProduct.Count <> 0 Then
    For Each objSWbemObjectEx_FireWallProduct In collSWbemObjectSet_FireWallProduct
        With objSWbemObjectEx_FireWallProduct
            WScript.Echo .instanceGuid & vbCrLf & .displayName & vbCrLf & .pathToSignedProductExe & vbCrLf & .pathToSignedReportingExe
        End With
    Next
Else
    WScript.Echo "Not found FireWall Products"
End If

Set collSWbemObjectSet_FireWallProduct = Nothing

WScript.Quit 0
