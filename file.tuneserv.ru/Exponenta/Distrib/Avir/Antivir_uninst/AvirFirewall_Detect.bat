rem ***************************************************************************
rem
rem AvirFirewall_Detect.bat
rem This Program Detects the Inner Antivirus and Firewalls at System
rem Program Uses WMIC to Get Inner Products
rem
rem ***************************************************************************
@echo off
wmic.exe /Node:. /NameSpace:\\Root\SecurityCenter2 Path AntiVirusProduct Get displayName, pathToSignedProductExe, pathToSignedReportingExe
wmic.exe /Node:. /NameSpace:\\Root\SecurityCenter2 Path FireWallProduct Get displayName, pathToSignedProductExe, pathToSignedReportingExe
wmic.exe /Node:. /NameSpace:\\Root\SecurityCenter2 Path AntiSpywareProduct Get displayName, pathToSignedProductExe, pathToSignedReportingExe