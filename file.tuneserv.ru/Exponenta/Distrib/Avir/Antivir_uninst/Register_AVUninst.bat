@echo on
rem *******************************************************
rem
rem The Command File for Scheduler Task Registration 
rem
rem Start the File with Elevated Privileges
rem
rem *******************************************************
@echo off

rem Set Variables
setlocal enableextensions enabledelayedexpansion

Rem Set System Variables

rem set Key=HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment
set Dest_DIR=C:\pub1\Util\Antivir_uninst
set Key=HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Run

echo -
echo Welcome to Antivirus Schedule UnInstaller!!
echo This program install the packet into directory %Dest_DIR%
echo -


echo Schedule Task..
echo -


%SystemRoot%\System32\reg.exe Add "!Key!" /v "RunAvirUninst" /t REG_SZ /d "C:\Windows\System32\cmd.exe /c C:\pub1\Util\Antivir_uninst\AVUnist_Sched.bat" /f

echo Installation is made with Success!
rem
