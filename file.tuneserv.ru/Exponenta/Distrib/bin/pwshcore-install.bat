@echo off
rem �������������� ���������� ��������� �������

SetLocal EnableExtensions EnableDelayedExpansion

rem Installation Directory

set PUB1=c:\pub1
set UTIL=c:\Util

rem Initialization Download Variables

set http_pref1=http
set http_host1=file.tuneserv.ru
set http_port1=80
set http_dir1=/Exponenta/

set ftp_pref1=ftp
set ftp_host1=files.tuneserv.ru
set ftp_port1=21
set ftp_user=user1550954_anonymous
set ftp_pass=Admin01234

rem HTTP WebDAV Host
set host=%http_pref1%://%http_host1%:%http_port1%%http_dir1%
rem ��������� �������
set LocalFolder=%PUB1%\Distrib
cd "%LocalFolder%"

rem Set Files

set PWSHX86=PowerShell-7.1.3-win-x86.msi
set PWSHX64=PowerShell-7.1.3-win-x64.msi

rem ��������� �� ���������� ������� WGET

DEL /F /Q "%LocalFolder%\%PWSHX86%"
DEL /F /Q "%LocalFolder%\%PWSHX64%"

rem PowerShell Core Download

"%LocalFolder%\wget.exe" %host%/%PWSHX86% -c -t 38 -w 120 -T 1800 -O "%LocalFolder%\%PWSHX86%"
"%LocalFolder%\wget.exe" %host%/%PWSHX64% -c -t 38 -w 120 -T 1800 -O "%LocalFolder%\%PWSHX64%"

rem Install Powershell Core
if not exist "%LocalFolder%\%PWSHX86%" goto pass_posh
if not exist "%LocalFolder%\%PWSHX64%" goto pass_posh
echo "Install PowerShell Core ..."
%SystemRoot%\system32\msiexec.exe /i "%LocalFolder%\%PWSHX86%" /norestart /QN /L*V %TEMP%\pwshx32.log ADD_EXPLORER_CONTEXT_MENU_OPENPOWERSHELL=1 ENABLE_PSREMOTING=1 REGISTER_MANIFEST=1
%SystemRoot%\system32\msiexec.exe /i "%LocalFolder%\%PWSHX64%" /norestart /QN /L*V %TEMP%\pwshx64.log ADD_EXPLORER_CONTEXT_MENU_OPENPOWERSHELL=1 ENABLE_PSREMOTING=1 REGISTER_MANIFEST=1
:pass_posh

rem ����� �� ��������. ������ - ������ �������.
exit /b 0
