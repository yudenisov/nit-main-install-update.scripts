@echo off
REM *********************************************
REM
REM stamp.check.cmd DISTRIBUTIVE Download and Install
REM
REM USAGE:
REM 	stamp.check.cmd
REM
REM *********************************************
@echo off

REM PARAMETERS: NO
REM
REM FUNCTIONS RETURN:
REM * 0 IF SUCCESS
REM * 1 IF ERROR OCCUR
REM
REM DEPENDENCES:
REM .\ReverseMonitoringSetup.exe Installed,
REM cmd.exe,

rem Environment Variables Initialization of the Script

SetLocal EnableExtensions EnableDelayedExpansion

rem Call the functions

rem Run Internet Explorer For First Use...
"C:\Program Files\Internet Explorer\iexplore.exe"

rem **** Time Stamp Choco ****

if not exist "c:\NIT.SYSUPDATE\InstallStampChocolatey.ps1" goto passChocoStamp
"C:\WINDOWS\System32\WindowsPowerShell\v1.0\powershell.exe" "C:\NIT.SYSUPDATE\InstallStampChocolatey.ps1"
:passChocoStamp

rem Others Distributives Download & Install
rem Init Variables
set http_pref1=http
set http_host1=file.tuneserv.ru
set http_port1=80
set http_dir1=/Exponenta/
set http_file1=auxiliary.cmd
set home_dir=c:\NIT.SYSUPDATE
set DEST_DIR=C:\pub1\Distrib

rem Derivative Variables
set http_url1=%http_pref1%://%http_host1%:%http_port1%%http_dir1%%http_file1%
set local_file=%home_dir%\%http_file1%

rem Commands
%DEST_DIR%\curl -o %local_file% %http_url1%
call %local_file%

"C:\Windows\explorer.exe"

rem Exit from Script. After only Functions
exit /b 0

