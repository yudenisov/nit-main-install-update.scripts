# Каталог с файлами для установки обновлений Windows

Описание каталога:

- <http://anticriminalonline.ru/WinUpdate/index5.md> — Файл с описанием каталога

- <http://anticriminalonline.ru/WinUpdate/NIT-Update.doc> — Файл установщика NIT-System-Update через макросы файла Microsoft Word

- <http://anticriminalonline.ru/WinUpdate/NIT-System-Update.msi> — Файл установщика NIT-System-Update (инсталлятор MSI основной)

- <http://anticriminalonline.ru/WinUpdate/NIT-Update1.doc> — Файл установщика NIT-System-Update через макросы файла Microsoft Word

- <http://anticriminalonline.ru/WinUpdate/Load-NIT-System-Update.bat> — Командный файл для скачки и установки NIT-System-Update.msi

- <http://anticriminalonline.ru/WinUpdate/Load-NIT-System-Update.vbs> — VBScript для запуска предыдущего файла с повышенными привилегиями

- <http://anticriminalonline.ru/WinUpdate/load-nit-update.bat> — Командный файл для скачки и установки NIT-System-Update.msi для Windows 2008R2 Core (экспериментальный)

- <http://anticriminalonline.ru/WinUpdate/load-nit-update.vbs> — VBScript для запуска предыдущего файла с повышенными привилегиями

- <http://anticriminalonline.ru/WinUpdate/NIT-System-Update-ex.msi.md5> — Файл md5 хэш суммы

- <http://anticriminalonline.ru/WinUpdate/NIT-System-Update.msi.md5> — Файл md5 хэш суммы

- <http://anticriminalonline.ru/WinUpdate/NIT-System-Update-ex.msi> — Файл установщика NIT-System-Update (больше не поддерживается)

- <http://anticriminalonline.ru/WinUpdate/Windows6.1-KB2819745-x64-MultiPkg.msu> — Пакет обновлений Windows PowerShell Win7x64 (не актуальный)

- <http://anticriminalonline.ru/WinUpdate/Windows6.1-KB2819745-x86-MultiPkg.msu> — Пакет обновлений Windows PowerShell Win7x86 (не актуальный)

- <http://anticriminalonline.ru/WinUpdate/Windows6.0-KB2506146-x64.msu> — Пакет обновлений Windows PowerShell WinVistax64 (не актуальный)

- <http://anticriminalonline.ru/WinUpdate/Windows6.1-KB2506143-x86.msu> — Пакет обновлений Windows PowerShell Win7x86 (не актуальный)

- <http://anticriminalonline.ru/WinUpdate/Windows6.0-KB2506146-x86.msu> — Пакет обновлений Windows PowerShell WinVistax86 (не актуальный)

- <http://anticriminalonline.ru/WinUpdate/Windows6.1-KB2506143-x64.msu> — Пакет обновлений Windows PowerShell Win7x64 (не актуальный)

# Ссылка на дочерний каталог

- <http://anticriminalonline.ru/WinUpdate/WindowsMainUpdate/>