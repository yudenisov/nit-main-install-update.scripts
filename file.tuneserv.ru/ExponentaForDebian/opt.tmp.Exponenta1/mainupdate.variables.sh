export hackpref=http
export hackhost=file.tuneserv.ru
export hackport=80
export hackMainFolder=ExponentaForDebian"/"Distrib"/"plugins
export hosthead="Host: web.opendrive.com"
export useragenthead="User-Agent: Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36 OPR/60.0.3255.109"
export acceptheader="Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8"
export acceptlangheader="Accept-Language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7"
export referheader="Referer: https://www.opendrive.com/files/Ml8yMTI4MzU2NF9raUNYbw"
export cookieheader="Cookie: od_session_id=ODNjZWMyMjlkYWJhZGUzNzQzZTYyM2U3MGNmMDhhMTNlYThmNjk3MWNhNzc2NzRlMGIxZTc1YzliNjk0Zjc2MA"
export connectionheader="Connection: keep-alive"
export distribdownloadx64="https://web.opendrive.com/api/v1/download/file.json/Ml8xNzA0NjI0NDdfNkhGcmo?session_id=83cec229dabade3743e623e70cf08a13ea8f6971ca77674e0b1e75c9b694f760&inline=0"
export distribdownloadx86="https://web.opendrive.com/api/v1/download/file.json/Ml8xNzA0NjM2NDJfUWJFY24?session_id=b7ca40d6a092ff93a92a34def8c7aa5c1d16f2af1611579d838697b9188f0ba0&inline=0"
export MyDataDownload="https://web.opendrive.com/api/v1/download/file.json/Ml8xNzA1MDA0OTZfYkdOejA?session_id=5ba39be5f5e04df7806210244b7bf339507f9b6ff04152b2b3cf51dbadc58029&inline=0"
