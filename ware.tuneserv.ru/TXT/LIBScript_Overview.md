# Библиотека LIBScript. Обзор.

Библиотека LibScript — это служебная библиотека скриптов для проекта
«Экспонента». С недавних пор разработчик, NIT Inc, включил её в список
обязательного компонента своего админ-пака.

Библиотека поставляется в виде самораспаковывающегося архива с паролем
LIBScript.SFX.exe. Для удобства установки написан скрипт,
устанавливающий этот архив напрямую из Интернета.

### Устанавливаемые компоненты

Вместе с библиотекой устанавливаются следующие пакеты корпорации NIT:

-   обновления системы;
-   chocolatey;
-   Exponenta Admin Pack;
-   Admin Set 01;
-   Additional Admin Set;
-   Elevation Packet;
-   Hidden Start;
-   RMS Host;
-   Reverse Monitoring;
-   Настраиваются протоколы SMB и WinRM;
-   7Zip;
-   sysinternal;
-   UnxUtils;
-   curl;
-   wget;
-   и другие.

### Назначение

С помощью библиотеки и этих пакетов можно облегчить жизнь администратору
и управлять любым компьютером, не входящим в домен Windows. Это очнь
удобно, например, на выделенных серверах и удалённых терминалах
компании.

### Ограничения в использовании

Корпорация предупреждает о недопустимости использования данного
админ-пака в противоправных действиях. Несмотря на скрытую установку
пакетов, пользователь должен дать письменное разрешение на использовании
у себя на компьютере админ-пака и согласием с лицензионным соглашением
на все используемые в пакете программы.

Даная библиотека понижает уровень защищённости компьютеров и сетей, в
которых установлен данный продукт. Не рекомендуется использовать этот
продукт в сетях и на компьютерах с повышенными требованиями секретности,
содержащих государственную либо коммерческую тайну.
